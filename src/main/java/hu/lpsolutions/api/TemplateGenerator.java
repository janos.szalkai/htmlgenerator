package hu.lpsolutions.api;

public interface TemplateGenerator {

    TemplateGenerator setDocTypeTag(String docTypeTag);
    TemplateGenerator setOpenHtmlTag(String openHtmlTag);
    TemplateGenerator setCharSet(String charSet);
    TemplateGenerator setCloseHtmlTag(String closeHtmlTag);
    TemplateGenerator setOpenHeadTag(String openHeadTag);
    TemplateGenerator setCloseHeadTag(String closeHeadTag);
    TemplateGenerator setOpenTitleTag(String openTitleTag);
    TemplateGenerator setCloseTitleTag(String closeTitleTag);
    TemplateGenerator setOpenBodyTag(String openBodyTag);
    TemplateGenerator setCloseBodyTag(String closeBodyTag);
    TemplateGenerator setOpenH1Tag(String openH1Tag);
    TemplateGenerator setCloseH1Tag(String closeH1Tag);
    TemplateGenerator setOpenPTag(String openPTag);
    TemplateGenerator setClosePTag(String closePTag);
    TemplateGenerator setOpenATagFirst(String openATagFirst);
    TemplateGenerator setOpenATagSecond(String openATagSecond);
    TemplateGenerator setCloseATag(String closeATag);
    TemplateGenerator setOpenTableTag(String openTableTag);
    TemplateGenerator setCloseTableTag(String closeTableTag);
    TemplateGenerator setOpenTrTag(String openTrTag);
    TemplateGenerator setCloseTrTag(String closeTrTag);
    TemplateGenerator setOpenTdTag(String openTdTag);
    TemplateGenerator setCloseTdTag(String closeTdTag);
    TemplateGenerator setTaskCreatorName(String taskCreatorName);
    TemplateGenerator setTitleContent(String titleContent);
    TemplateGenerator setH1Content(String h1Content);
    TemplateGenerator setAFirstContent(String aFirstContent);
    TemplateGenerator setPContent(String pContent);
    TemplateGenerator setTdContentName(String tdContentName);
    TemplateGenerator setTdContentEmail(String tdContentEmail);
    TemplateGenerator setTaskCreatorEmail(String taskCreatorEmail);
    TemplateGenerator setASecondContent(String aSecondContent);
    TemplateGenerator create();
}
