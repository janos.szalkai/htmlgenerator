package hu.lpsolutions.api;

public interface Task {
    Task setUrl(String url);
    Task setCreator(String creator);
    Task setCreatorEmail(String creatorEmail);
    Task create();

}
