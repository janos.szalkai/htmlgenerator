package hu.lpsolutions.impl;

import hu.lpsolutions.api.Task;

public class TaskImpl implements Task {
    private String url;
    private String creator;
    private String creatorEmail;

    @Override
    public Task setUrl(String url) {
        this.url = url;
        return this;
    }

    @Override
    public Task setCreator(String creator) {
        this.creator = creator;
        return this;
    }

    @Override
    public Task setCreatorEmail(String creatorEmail) {
        this.creatorEmail = creatorEmail;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public String getCreator() {
        return creator;
    }

    public String getCreatorEmail() {
        return creatorEmail;
    }

    @Override
    public Task create() {
        return new TaskImpl()
                .setUrl("http://www.gitlab.com/janos.szalkai/htmlgenerator")
                .setCreator("Szálkai János")
                .setCreatorEmail("janos.szalkai@gmail.com");
    }
}
