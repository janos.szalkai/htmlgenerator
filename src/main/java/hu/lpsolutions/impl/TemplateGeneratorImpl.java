package hu.lpsolutions.impl;

import hu.lpsolutions.api.TemplateGenerator;

public class TemplateGeneratorImpl implements TemplateGenerator {

    private String docTypeTag;
    private String charSet;
    private String openHtmlTag;
    private String closeHtmlTag;
    private String openHeadTag;
    private String closeHeadTag;
    private String openTitleTag;
    private String closeTitleTag;
    private String openBodyTag;
    private String closeBodyTag;
    private String openH1Tag;
    private String closeH1Tag;
    private String openPTag;
    private String closePTag;
    private String openATagFirst;
    private String openATagSecond;
    private String closeATag;
    private String openTableTag;
    private String closeTableTag;
    private String openTrTag;
    private String closeTrTag;
    private String openTdTag;
    private String closeTdTag;
    private String taskCreatorName;
    private String titleContent;
    private String h1Content;
    private String aFirstContent;
    private String pContent;
    private String tdContentName;
    private String tdContentEmail;
    private String taskCreatorEmail;
    private String aSecondContent;


    @Override
    public TemplateGenerator setDocTypeTag(String docTypeTag) {
        this.docTypeTag = docTypeTag;
        return this;
    }

    @Override
    public TemplateGenerator setOpenHtmlTag(String openHtmlTag) {
        this.openHtmlTag = openHtmlTag;
        return this;
    }

    @Override
    public TemplateGenerator setCharSet(String charSet) {
        this.charSet = charSet;
        return this;
    }

    @Override
    public TemplateGenerator setCloseHtmlTag(String closeHtmlTag) {
        this.closeHtmlTag = closeHtmlTag;
        return this;
    }

    @Override
    public TemplateGenerator setOpenHeadTag(String openHeadTag) {
        this.openHeadTag = openHeadTag;
        return this;
    }

    @Override
    public TemplateGenerator setCloseHeadTag(String closeHeadTag) {
        this.closeHeadTag = closeHeadTag;
        return this;
    }

    @Override
    public TemplateGenerator setOpenTitleTag(String openTitleTag) {
        this.openTitleTag = openTitleTag;
        return this;
    }

    @Override
    public TemplateGenerator setCloseTitleTag(String closeTitleTag) {
        this.closeTitleTag = closeTitleTag;
        return this;
    }

    @Override
    public TemplateGenerator setOpenBodyTag(String openBodyTag) {
        this.openBodyTag = openBodyTag;
        return this;
    }

    @Override
    public TemplateGenerator setCloseBodyTag(String closeBodyTag) {
        this.closeBodyTag = closeBodyTag;
        return this;
    }

    @Override
    public TemplateGenerator setOpenH1Tag(String openH1Tag) {
        this.openH1Tag = openH1Tag;
        return this;
    }

    @Override
    public TemplateGenerator setCloseH1Tag(String closeH1Tag) {
        this.closeH1Tag = closeH1Tag;
        return this;
    }

    @Override
    public TemplateGenerator setOpenPTag(String openPTag) {
        this.openPTag = openPTag;
        return this;
    }

    @Override
    public TemplateGenerator setClosePTag(String closePTag) {
        this.closePTag = closePTag;
        return this;
    }

    @Override
    public TemplateGenerator setOpenATagFirst(String openATagFirst) {
        this.openATagFirst = openATagFirst;
        return this;
    }

    @Override
    public TemplateGenerator setOpenATagSecond(String openATagSecond) {
        this.openATagSecond = openATagSecond;
        return this;
    }

    @Override
    public TemplateGenerator setCloseATag(String closeATag) {
        this.closeATag = closeATag;
        return this;
    }

    @Override
    public TemplateGenerator setOpenTableTag(String openTableTag) {
        this.openTableTag = openTableTag;
        return this;
    }

    @Override
    public TemplateGenerator setCloseTableTag(String closeTableTag) {
        this.closeTableTag = closeTableTag;
        return this;
    }

    @Override
    public TemplateGenerator setOpenTrTag(String openTrTag) {
        this.openTrTag = openTrTag;
        return this;
    }

    @Override
    public TemplateGenerator setCloseTrTag(String closeTrTag) {
        this.closeTrTag = closeTrTag;
        return this;
    }

    @Override
    public TemplateGenerator setOpenTdTag(String openTdTag) {
        this.openTdTag = openTdTag;
        return this;
    }

    @Override
    public TemplateGenerator setCloseTdTag(String closeTdTag) {
        this.closeTdTag = closeTdTag;
        return this;
    }

    @Override
    public TemplateGenerator setTaskCreatorName(String taskCreatorName) {
        this.taskCreatorName = taskCreatorName;
        return this;
    }

    @Override
    public TemplateGenerator setTitleContent(String titleContent) {
        this.titleContent = titleContent;
        return this;
    }

    @Override
    public TemplateGenerator setH1Content(String h1Content) {
        this.h1Content = h1Content;
        return this;
    }

    @Override
    public TemplateGenerator setAFirstContent(String aFirstContent) {
        this.aFirstContent = aFirstContent;
        return this;
    }

    @Override
    public TemplateGenerator setPContent(String pContent) {
        this.pContent = pContent;
        return this;
    }

    @Override
    public TemplateGenerator setTdContentName(String tdContentName) {
        this.tdContentName = tdContentName;
        return this;
    }

    @Override
    public TemplateGenerator setTdContentEmail(String tdContentEmail) {
        this.tdContentEmail = tdContentEmail;
        return this;
    }

    @Override
    public TemplateGenerator setTaskCreatorEmail(String taskCreatorEmail) {
        this.taskCreatorEmail = taskCreatorEmail;
        return this;
    }

    @Override
    public TemplateGenerator setASecondContent(String aSecondContent) {
        this.aSecondContent = aSecondContent;
        return this;
    }

    @Override
    public TemplateGenerator create() {
        return new TemplateGeneratorImpl()
                .setOpenHtmlTag("<html>")
                .setDocTypeTag("<!DOCTYPE html>")
                .setCharSet("<meta charset=\"UTF-8\">")
                .setOpenHeadTag("<head>")
                .setOpenTitleTag("<title>")
                .setTaskCreatorName("${task.creator}")
                .setTitleContent(" - Teszt Feladat")
                .setCloseTitleTag("</title>")
                .setCloseHeadTag("</head>")
                .setOpenBodyTag("<body>")
                .setOpenH1Tag("<h1>")
                .setH1Content("Teszt Feladat")
                .setCloseH1Tag("</h1>")
                .setOpenPTag("<p>")
                .setOpenATagFirst("<a href=\"${task.url}\">")
                .setAFirstContent("Megoldás")
                .setCloseATag("</a>")
                .setClosePTag("</p>")
                .setOpenPTag("<p>")
                .setPContent("A feladat elkészítőjének adatai")
                .setClosePTag("</p>")
                .setOpenTableTag("<table border=\"1px solid black\">")
                .setOpenTrTag("<tr>")
                .setOpenTdTag("<td>")
                .setTdContentName("Név")
                .setCloseTdTag("</td>")
                .setOpenTdTag("<td>")
                .setTaskCreatorName("${task.creator}")
                .setCloseTdTag("</td>")
                .setCloseTrTag("</tr>")
                .setOpenTrTag("<tr>")
                .setOpenTdTag("<td>")
                .setTdContentEmail("Elérhetőség")
                .setCloseTdTag("</td>")
                .setOpenTdTag("<td>")
                .setTaskCreatorEmail("${task.creatorEmail}")
                .setCloseTdTag("</td>")
                .setCloseTrTag("</tr>")
                .setCloseTableTag("</table>")
                .setOpenATagSecond("<a href=\"http://lpsolutions.hu\">")
                .setASecondContent("L&PSolutions")
                .setCloseATag("</a>")
                .setCloseBodyTag("</body>")
                .setCloseHtmlTag("</html>");
    }

    @Override
    public String toString() {
        return docTypeTag + "\n" +
                charSet + "\n" +
                openHtmlTag + "\n" +
                "\t" + openHeadTag + "\n" +
                "\t" + "\t" + openTitleTag + taskCreatorName + titleContent + closeTitleTag + "\n" +
                "\t" + closeHeadTag + "\n" +
                "\t" + openBodyTag + "\n" +
                "\t" + "\t" + openH1Tag + h1Content + closeH1Tag + "\n" +
                "\t" + "\t" + openPTag + openATagFirst + aFirstContent + closeATag + closePTag + "\n" +
                "\t" + "\t" + openPTag + pContent + closePTag + "\n" +
                "\t" + "\t" + openTableTag + "\n" +
                "\t" + "\t" + "\t" + openTrTag + "\n" +
                "\t" + "\t" + "\t" + "\t" + openTdTag + tdContentName + closeTdTag + "\n" +
                "\t" + "\t" + "\t" + "\t" + openTdTag + taskCreatorName + closeTdTag + "\n" +
                "\t" + "\t" + "\t" + closeTrTag + "\n" +
                "\t" + "\t" + "\t" + openTrTag + "\n" +
                "\t" + "\t" + "\t" + "\t" + openTdTag + tdContentEmail + closeTdTag + "\n" +
                "\t" + "\t" + "\t" + "\t" + openTdTag + taskCreatorEmail + closeTdTag + "\n" +
                "\t" + "\t" + "\t" + closeTrTag + "\n" +
                "\t" + "\t" + closeTableTag + "\n" +
                "\t" + "\t" + openATagSecond + aSecondContent + closeATag + "\n" +
                "\t" + closeBodyTag + "\n" +
                closeHtmlTag;
    }
}
