package hu.lpsolutions;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import hu.lpsolutions.api.Task;
import hu.lpsolutions.api.TemplateGenerator;
import hu.lpsolutions.impl.TaskImpl;
import hu.lpsolutions.impl.TemplateGeneratorImpl;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class Main {
    public static void main(String[] args) throws IOException, TemplateException {
        BufferedWriter templateFTL = new BufferedWriter(new FileWriter("C:\\projects\\freemaker\\src\\main\\resources\\template.ftl"));
        TemplateGenerator templateGenerator = new TemplateGeneratorImpl();
        templateFTL.write(templateGenerator.create().toString());
        templateFTL.close();

        Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
        cfg.setDirectoryForTemplateLoading(new File("C:\\projects\\freemaker\\src\\main\\resources"));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);
        cfg.setWrapUncheckedExceptions(true);
        cfg.setFallbackOnNullLoopVariable(false);
        cfg.setSQLDateAndTimeTimeZone(TimeZone.getDefault());


        Map<String, Task> root = new HashMap<>();
        Task task = new TaskImpl().create();
        root.put("task", task);


        Template temp = cfg.getTemplate("template.ftl");

        Writer out = new OutputStreamWriter(Files.newOutputStream(Paths.get("target/freemaker.html")));

        temp.process(root, out);

        new File("C:\\projects\\freemaker\\src\\main\\resources\\template.ftl").delete();
    }
}
